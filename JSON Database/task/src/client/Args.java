package client;

import com.beust.jcommander.Parameter;
public class Args {
    @Parameter(names = {"-t"}, description = "Type of command")
    String type;

    @Parameter(names = {"-k"}, description = "key")
    String key;

    @Parameter(names = {"-v"}, description = "String message")
    String value;

    @Parameter(names = {"-in"}, description = "Inout request file name")
    String fileName;
}
