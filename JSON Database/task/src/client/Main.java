package client;

import com.beust.jcommander.JCommander;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import dto.ClientJson;
import dto.Request;
import dto.RequestJson;
import util.JsonUtil;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static final String IP_ADDRESS = "127.0.0.1";
    private static final int PORT = 23456;

    private static final int NUM = 12;

    public static void main(String[] args) {

        //test();
        Args argv = new Args();
        JCommander.newBuilder()
                .addObject(argv)
                .build()
                .parse(args);
        System.out.println("Client started!");
        RequestJson request = new RequestJson();
        if (!Objects.isNull(argv.fileName)) {
            ClientJson clientJson = buildRequest(argv.fileName);
            sendRequest(clientJson);
        } else if (argv.type.equals("exit")){
            request.setType("exit");
            sendRequest(request);
        } else {
            ClientJson clientJson = buildRequest(argv);
            sendRequest(clientJson);
        }

    }


    private static void test( ) {
        Request requestOne = new Request();
        requestOne.setType("get");
        requestOne.setKey("1");
        Request requestTwo = new Request();
        requestTwo.setType("set");
        requestTwo.setKey("1");
        requestTwo.setValue("Hello world!");
        Request requestThree = new Request();
        requestThree.setType("set");
        requestThree.setKey("1");
        requestThree.setValue("HelloWorld!");
        Request requestFour = new Request();
        requestFour.setType("get");
        requestFour.setKey("1");
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> {
            //sendRequest(requestOne);
        });
        executorService.execute(() -> {
            //sendRequest(requestTwo);
        });
        executorService.execute(() -> {
           // sendRequest(requestThree);
        });
        executorService.execute(() -> {
           // sendRequest(requestFour);
        });
    }

    private static void sendRequest(RequestJson request) {
        try (Socket socket = new Socket(IP_ADDRESS, PORT)){
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            if (request.getType().equalsIgnoreCase("exit")) {
               // outputStream.writeUTF(JsonUtil.toJson(Request.EXIT));
                return;
            }
            String requestJson = JsonUtil.toJson(request);
            outputStream.writeUTF(requestJson);
            System.out.printf("Sent: %s%n", requestJson);
            String receivedMessage = inputStream.readUTF();
            System.out.println(String.format("Received: %s", receivedMessage));
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static void sendRequest(ClientJson request) {
        try (Socket socket = new Socket(IP_ADDRESS, PORT)){
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            if (request.getType().equalsIgnoreCase("exit")) {
                //outputStream.writeUTF(JsonUtil.toJson(Request.EXIT));
                return;
            }
            outputStream.writeUTF(request.getJson());
            System.out.printf("Sent: %s%n", request.getJson());
            String receivedMessage = inputStream.readUTF();
            System.out.println(String.format("Received: %s", receivedMessage));
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static ClientJson buildRequest(Args args) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", args.type);
        jsonObject.addProperty("key", args.key);
        jsonObject.addProperty("value", args.value);
        String json = new Gson().toJson(jsonObject);
        ClientJson request = new ClientJson();
        request.setType(args.type);
        request.setJson(json);
        return request;
    }

    private static ClientJson buildRequest(String fileName) {

        String fullPath = "/home/lakshitha/IdeaProjects/JSON Database1/JSON Database/task/src/client/data/" + fileName;
        Path filFullPath  = Path.of(fullPath);
        try {
            String json = Files.readString(filFullPath);
            JsonElement requestJsonElement = new Gson().fromJson(json, JsonElement.class);
            String type = requestJsonElement.getAsJsonObject().get("type").getAsString();
            ClientJson clientJson = new ClientJson();
            clientJson.setType(type);
            clientJson.setJson(json.replaceAll("\\n+", ""));
           return clientJson;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static String loadRequest(String fileName) {

        String fullPath = "/home/lakshitha/IdeaProjects/JSON Database1/JSON Database/task/src/client/data/" + fileName;
        Path filFullPath  = Path.of(fullPath);
        try {
            return Files.readString(filFullPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
