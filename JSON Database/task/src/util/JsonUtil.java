package util;

import com.google.gson.*;
import dto.ClientJson;
import dto.Request;
import dto.RequestJson;
import dto.Response;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class JsonUtil {

    public static String toJson(RequestJson request) {
        Gson gson = new GsonBuilder()
                .create();
        return gson.toJson(request);
    }

//    public static String toJson(ClientJson request) {
//        Gson gson = new GsonBuilder()
//                .create();
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("key", request.getKey());
//        jsonObject.addProperty("type", request.getType());
//        jsonObject.add("value", request.getValue());
//        return gson.toJson(request);
//    }

    public static String toJson(Response response) {
        Gson gson = new GsonBuilder()
                .create();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("response", response.getResponse());
        if (response.getReason() != null && !response.getReason().isBlank()) {
            jsonObject.addProperty("reason", response.getResponse());
        }
        if (response.getValue() != null) {
            jsonObject.add("value", response.getValue());
        }
        return gson.toJson(jsonObject);
    }

    public static Request fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Request.class);
    }

    public static JsonElement get(JsonElement jsonElement, String... args) {

        JsonObject js = jsonElement.getAsJsonObject();
        if (args.length == 0) {
            return jsonElement;
        } else if (args.length == 1) {
            return js.get(args[0]);
        } else {

            JsonElement element = js.get(args[0]);
            return get(element, Arrays.copyOfRange(args, 1, args.length));
        }
    }

    public static RequestJson convert(String receivedJson) {

        JsonElement requestJsonElement = new Gson().fromJson(receivedJson, JsonElement.class);
        String type = requestJsonElement.getAsJsonObject().get("type").getAsString();
        JsonElement keyJsonElement = requestJsonElement.getAsJsonObject().get("key");
        RequestJson requestJson = new RequestJson();
        if (keyJsonElement.isJsonPrimitive()) {
            requestJson.setKey(keyJsonElement.getAsString());
        } else {
            requestJson.setKey(keyJsonElement.getAsJsonArray().toString());
        }
        JsonElement valueJsonElement = requestJsonElement.getAsJsonObject().get("value");
        if (valueJsonElement != null && valueJsonElement.isJsonObject()) {
            requestJson.setValue(valueJsonElement.getAsJsonObject());
        } else if (valueJsonElement != null ){
            JsonPrimitive primitive = new JsonPrimitive(valueJsonElement.getAsString());
            requestJson.setValue(primitive);
        }
        requestJson.setType(type);
        return requestJson;

    }

    private static void set(String key, JsonElement value) throws IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("key", key);
        jsonObject.add("value", value);
        Gson gson = new Gson();
        FileWriter writer = new FileWriter("test.json");
        gson.toJson(jsonObject, writer);

        writer.close();
    }

    public static String[] toKeyArray(String key) {
        key = key.trim();
        if (key.contains("[")) {

            Gson gson = new Gson();
            JsonArray jsonArray = gson.fromJson(key, JsonArray.class);
            String[] map = new String[jsonArray.size()];
            for (int i = 0; i < jsonArray.size(); i++) {
                map[i] = jsonArray.get(i).getAsString();
            }
            return map;
        } else {
            return new String[]{key};
        }
    }
}