package database;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dto.DbEntry;
import dto.Response;
import util.JsonUtil;

import java.io.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Database {
    private Map<String, String> data;
    Lock readLock;
    Lock writeLock;

    private final static String DB_FILE_FULL_PATH = "/home/lakshitha/IdeaProjects/JSON Database1/JSON Database/task/src/server/data/db.json";
    public Database() {
        this.data = new HashMap<>();
        ReadWriteLock lock = new ReentrantReadWriteLock();
        this.readLock = lock.readLock();
        this.writeLock = lock.writeLock();
    }

    public Response execute(String command, String keys, JsonElement value) {
        switch (command) {
            case "get":
                System.out.println("=========" + keys);
                JsonElement storedValue = get(JsonUtil.toKeyArray(keys));
                Response response = new Response();
                response.setResponse("OK");
                response.setValue(storedValue);
                return response;
            case "set":
                set(keys, value);
                return Response.OK;
            case "delete":
                delete(keys);
                return Response.OK;
            default:
                return Response.ERROR;
        }
    }


    private JsonElement get(String[] keyMatcher) {

        try {
            JsonElement valuesJsonElement = new Gson().fromJson(new FileReader(DB_FILE_FULL_PATH), JsonElement.class);
            JsonElement valueJsonElement = valuesJsonElement.getAsJsonObject().get("value");
            String key = valuesJsonElement.getAsJsonObject().get("key").getAsString();
            if (key.equalsIgnoreCase(keyMatcher[0])) {
                JsonElement value = JsonUtil.get(valueJsonElement, Arrays.copyOfRange(keyMatcher, 1, keyMatcher.length));
                return value;
            }
            return null;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }


//        readLock.lock();
//        File dbFile = new File(DB_FILE_FULL_PATH);
//        String currentLine;
//        Gson gson = new Gson();
//        try (BufferedReader reader = new BufferedReader(new FileReader(dbFile))) {
//            while((currentLine = reader.readLine()) != null) {
//                DbEntry dbEntry = gson.fromJson(currentLine, DbEntry.class);
//                if (!Objects.isNull(dbEntry) && !Objects.isNull(dbEntry.getKey()) && dbEntry.getKey().equals(key)) {
//                    return dbEntry.getValue();
//                }
//            }
//
//        }catch (FileNotFoundException e) {
//           throw new RuntimeException(e);
//       } catch (IOException e) {
//           throw new RuntimeException(e);
//       } finally {
//            readLock.unlock();
//       }
//        throw new IllegalArgumentException();
    }

    private void set(String key, JsonElement value) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("key", key);
        jsonObject.add("value", value);
        Gson gson = new Gson();
        try (FileReader reader = new FileReader(DB_FILE_FULL_PATH)){
            JsonObject rootObject = gson.fromJson(reader, JsonObject.class);
            String[] keys = JsonUtil.toKeyArray(key);
            if (keys.length == 1) {
                try (FileWriter writer = new FileWriter(DB_FILE_FULL_PATH);){
                    gson.toJson(jsonObject, writer);
                }
            } else {
                JsonObject toEdit = rootObject.get("value").getAsJsonObject();
                for (String s : Arrays.copyOfRange(keys, 1, keys.length - 1)) {
                    JsonObject jsonElement = toEdit.get(s).getAsJsonObject();
                    if (jsonElement != null) {
                        toEdit = jsonElement;
                    } else {
                        break;
                    }
                }
                toEdit.add(keys[keys.length - 1], value);
                try (FileWriter writer = new FileWriter(DB_FILE_FULL_PATH);){
                    gson.toJson(rootObject, writer);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }



        //data.put(key, value);
        /*String uid = UUID.randomUUID().toString();
        System.out.println(uid + "+++++++++++++ " + key + "-------- " + value);
        if (key.isBlank() || value.isBlank()) {
            return;
        }
        DbEntry entry = new DbEntry(key, value);
        Gson gson = new Gson();
        File dbFile = new File(DB_FILE_FULL_PATH);
        File tmpFile = new File("_temp_");
        try (BufferedWriter tempFile = new BufferedWriter(new FileWriter(tmpFile, false));
             BufferedReader reader = new BufferedReader(new FileReader(dbFile))) {
            writeLock.lock();
            String currentLine;
            boolean fileEmpty = true;
            boolean matchFound = false;
            while((currentLine = reader.readLine()) != null) {
                if (currentLine.isBlank()) {
                    continue;
                }
                DbEntry currentEntry = gson.fromJson(currentLine, DbEntry.class);
                if (currentEntry.getKey() == null || currentEntry.getKey().isBlank()) {
                    System.out.println(uid + "empty");
                    continue;
                } else if (currentEntry.getKey().equals(key)) {
                    String json = gson.toJson(entry);
                    System.out.println(uid + "======-------------- " + json);
                    tempFile.write(json + System.getProperty("line.separator"));
                    matchFound = true;
                } else {
                    System.out.println(uid + "======>>>>>>>>>>> " + currentLine);
                    tempFile.write(currentLine + System.getProperty("line.separator"));
                }
                fileEmpty = false;
            }
            if (fileEmpty || !matchFound) {
                String json = gson.toJson(entry);
                tempFile.write(json + System.getProperty("line.separator"));
                System.out.println(uid + "00000000000 " + json);
            }
            tempFile.flush();
            tmpFile.renameTo(dbFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            writeLock.unlock();
        }*/
    }

    private void delete(String key) {

        try (FileReader reader = new FileReader(DB_FILE_FULL_PATH)) {
            Gson gson = new Gson();
            JsonObject rootObject = gson.fromJson(reader, JsonObject.class);
            String[] keys = JsonUtil.toKeyArray(key);
            if (keys.length == 1) {

            } else {
                JsonObject toDelete = rootObject.get("value").getAsJsonObject();
                for (String s : Arrays.copyOfRange(keys, 1, keys.length - 1)) {
                    JsonObject jsonElement = toDelete.get(s).getAsJsonObject();
                    if (jsonElement != null) {
                        toDelete = jsonElement;
                    } else {
                        break;
                    }
                }
                toDelete.remove(keys[keys.length - 1]);
                try (FileWriter writer = new FileWriter(DB_FILE_FULL_PATH);){
                    gson.toJson(rootObject, writer);
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

//    public static void main(String[] args) {
//
//        File dbFile = new File(DB_FILE_FULL_PATH);
//        String currentLine;
//        try (BufferedReader reader = new BufferedReader(new FileReader(dbFile))) {
//
//            while((currentLine = reader.readLine()) != null) {
//
//                System.out.println(currentLine);
//            }
//
//        }catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } finally {
//        }
//
//    }


}

