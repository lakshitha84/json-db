package server;

import database.Database;
import dto.Request;
import dto.RequestJson;
import dto.Response;
import util.JsonUtil;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static final int SERVER_PORT = 23456;

    private static final int NUM = 12;

    public static void main(String[] args) {
        Database database = new Database();
        ExecutorService executor = Executors.newCachedThreadPool();
        try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT)) {
            System.out.println("Server started!");
            while (true) {
                Socket socket = serverSocket.accept();
                DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
                String message = inputStream.readUTF();
                System.out.printf("Received: %s", message);
                System.out.println();
                RequestJson request = JsonUtil.convert(message);
                if (request.getType().equals("exit")) {
                    outputStream.writeUTF(JsonUtil.toJson(Response.OK));
                    socket.close();
                    break;
                }
                executor.execute(() -> {
                    try {
                        handleClient(database, outputStream, request, socket);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            executor.shutdown();
        } catch (IOException e) {
           e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static boolean handleClient(Database database, DataOutputStream outputStream, RequestJson request, Socket socket) throws IOException {


        try {
            if (request.getType().equalsIgnoreCase("get") || request.getType().equalsIgnoreCase("delete")) {
                Response response = database.execute(request.getType(), request.getKey(), null);
                outputStream.writeUTF(JsonUtil.toJson(response));
            } else {
                Response response = database.execute(request.getType(), request.getKey(), request.getValue());
                String json = JsonUtil.toJson(response);
                outputStream.writeUTF(json);
            }
        } catch (IllegalArgumentException e) {
            outputStream.writeUTF(JsonUtil.toJson(Response.ERROR));
        } finally {
            socket.close();
        }
        return false;
    }
}

