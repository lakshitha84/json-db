package dto;

import com.google.gson.JsonElement;

public class Response {
    public static final Response OK = new Response("OK");
    public static final Response ERROR = new Response("ERROR", "No such key");
    private String response;
    private JsonElement value;
    private String reason;

    public Response() {
    }

    public Response(String response, String reason) {
        this.response = response;
        this.reason = reason;
    }

    public Response(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public JsonElement getValue() {
        return value;
    }

    public void setValue(JsonElement value) {
        this.value = value;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}